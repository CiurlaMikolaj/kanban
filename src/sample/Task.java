package sample;

import javafx.beans.property.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;

class Task implements Serializable {

    private transient StringProperty taskName;
    private transient StringProperty taskDescription;
    private transient StringProperty prioryty;
    private transient ObjectProperty<LocalDate> expDate;

    private void initInstance() {
        taskName =new SimpleStringProperty();
        taskDescription =new SimpleStringProperty();
        prioryty =new SimpleStringProperty();
        expDate =new SimpleObjectProperty<>();
    }

    Task(){
        //this(null, null);
        this.taskName = new SimpleStringProperty(null);
        this.taskDescription = new SimpleStringProperty(null);
        this.prioryty = new SimpleStringProperty(null);
        this.expDate = new SimpleObjectProperty<>(null);
    }

    public Task(String name, String description, String priority, LocalDate expDate){
        // Some initial dummy data, just for convenient testing.
        this.taskName = new SimpleStringProperty(name);
        this.taskDescription = new SimpleStringProperty(description);
        this.prioryty = new SimpleStringProperty(priority);
        this.expDate = new SimpleObjectProperty<LocalDate>(expDate);
    }

    String getTaskName() {
        return taskName.get();
    }

    void setTaskName(String name) {
        this.taskName.set(name);
    }

    StringProperty taskNameProperty() {
        return taskName;
    }

    String getTaskDescription() {
        return taskDescription.get();
    }

    void setTaskDescription(String description) {
        this.taskDescription.set(description);
    }

    LocalDate getExpDate() {
        return expDate.get();
    }

    void setExpDate(LocalDate expDate) {
        this.expDate.set(expDate);
    }

    String getPriority(){
        return prioryty.get();
    }

    void setPriority(String prioryty){
        this.prioryty.set(prioryty);
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        s.defaultWriteObject();
        s.writeUTF((taskName).getValueSafe());
        s.writeUTF((taskDescription).getValueSafe());
        s.writeUTF((prioryty).getValueSafe());
        s.writeObject((expDate).get());
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        initInstance();
        (taskName).setValue(s.readUTF());
        (taskDescription).setValue(s.readUTF());
        (prioryty).setValue(s.readUTF());
        ((ObjectProperty)expDate).setValue(s.readObject());
    }

}
