package sample;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class TaskEditDialogController {

    @FXML
    private TextField titleField;
    @FXML
    private ComboBox priorityField;
    @FXML
    private DatePicker dateField;
    @FXML
    private TextArea descriptionField;

    private Stage dialogStage;
    private Task task;
    private boolean okClicked = false;

    @FXML
    private void initialize() {
        priorityField.getItems().add("LOW");
        priorityField.getItems().add("MEDIUM");
        priorityField.getItems().add("HIGH");
    }

    void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    void setTask(Task task) {
        this.task = task;

        titleField.setText(task.getTaskName());
        descriptionField.setText(task.getTaskDescription());
        priorityField.setValue(task.getPriority());
        dateField.setValue(task.getExpDate());
    }

    boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleOk() {
        if (isInputValid()) {
            task.setTaskDescription(descriptionField.getText());
            task.setPriority(priorityField.getValue().toString());
            task.setExpDate(dateField.getValue());
            task.setTaskName(titleField.getText());

            okClicked = true;
            dialogStage.close();
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (titleField.getText() == null || titleField.getText().length() == 0) {
            errorMessage += "No valid description!\n";
        }
        if (descriptionField.getText() == null || descriptionField.getText().length() == 0) {
            errorMessage += "No valid title!\n";
        }
        if (dateField.getValue() == null || dateField.getValue().toString().length() == 0) {
            errorMessage += "No valid date!\n";
        }
        if (priorityField.getValue() == null) {
            errorMessage += "No valid priority!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}
