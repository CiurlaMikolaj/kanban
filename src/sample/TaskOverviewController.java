package sample;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class TaskOverviewController {

    @FXML
    TableView<Task> taskTable;
    @FXML
    TableView<Task> taskTable2;
    @FXML
    TableView<Task> taskTable3;
    @FXML
    TableColumn<Task, String> taskColumn;
    @FXML
    TableColumn<Task, String> taskColumn2;
    @FXML
    TableColumn<Task, String> taskColumn3;

    private Main mainApp;

    public TaskOverviewController(){}

    @FXML
    private void initialize() {
        // Initialize the task table with the column.
        taskColumn.setCellValueFactory(cellData -> cellData.getValue().taskNameProperty());
        taskColumn2.setCellValueFactory(cellData -> cellData.getValue().taskNameProperty());
        taskColumn3.setCellValueFactory(cellData -> cellData.getValue().taskNameProperty());

        setNewTooltip(taskTable, taskColumn);
        setNewTooltip(taskTable2, taskColumn2);
        setNewTooltip(taskTable3, taskColumn3);
    }

    @FXML
    private void handleDeleteTask() {
        int selectedIndex = taskTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            taskTable.getItems().remove(selectedIndex);
        } else {
            noSelectionAlert();
        }
    }

    @FXML
    private void handleDeleteTask2() {
        int selectedIndex = taskTable2.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            taskTable2.getItems().remove(selectedIndex);
        } else {
            noSelectionAlert();
        }
    }

    @FXML
    private void handleDeleteTask3() {
        int selectedIndex = taskTable3.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            taskTable3.getItems().remove(selectedIndex);
        } else {
            noSelectionAlert();
        }
    }

    @FXML
    private void handleNewTask() throws Exception{
        Task tempTask = new Task();
        boolean okClicked = mainApp.showTaskEditDialog(tempTask);
        if (okClicked) {
            mainApp.getTaskData().add(tempTask);
        }
    }

    @FXML
    private void handleEditPerson() throws Exception{
        Task selectedTask = taskTable.getSelectionModel().getSelectedItem();
        if (selectedTask != null) {
            mainApp.showTaskEditDialog(selectedTask);
            setNewTooltip(taskTable,taskColumn);
        } else {
            noSelectionAlert();
        }
    }

    @FXML
    private void handleEditPerson2() throws Exception{
        Task selectedTask = taskTable2.getSelectionModel().getSelectedItem();
        if (selectedTask != null) {
            mainApp.showTaskEditDialog(selectedTask);
            setNewTooltip(taskTable2,taskColumn2);
        } else {
            noSelectionAlert();
        }
    }

    @FXML
    private void handleEditPerson3() throws Exception{
        Task selectedTask = taskTable3.getSelectionModel().getSelectedItem();
        if (selectedTask != null) {
            mainApp.showTaskEditDialog(selectedTask);
            setNewTooltip(taskTable3,taskColumn3);
        } else {
            noSelectionAlert();
        }
    }

    @FXML
    public void handleMovingRight(){
        Task selectedTask = taskTable.getSelectionModel().getSelectedItem();
        Task selectedTask2 = taskTable2.getSelectionModel().getSelectedItem();
        Task selectedTask3 = taskTable3.getSelectionModel().getSelectedItem();

        if (selectedTask != null) {
            mainApp.getTaskData2().add(selectedTask);
            mainApp.getTaskData().remove(selectedTask);
            taskTable.getSelectionModel().clearSelection();
        }
        else if(selectedTask2 != null){
            mainApp.getTaskData3().add(selectedTask2);
            mainApp.getTaskData2().remove(selectedTask2);
            taskTable2.getSelectionModel().clearSelection();
        }
        else if(selectedTask3 != null){
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Task Can't Go Further");
            alert.setHeaderText("No More Progress Tables");
            alert.setContentText("Please select another task in the tables or move this task back");

            alert.showAndWait();
            taskTable3.getSelectionModel().clearSelection();
        }
        else{
            noSelectionAlert();
        }
    }

    @FXML
    public void handleMovingLeft() {
        Task selectedTask = taskTable.getSelectionModel().getSelectedItem();
        Task selectedTask2 = taskTable2.getSelectionModel().getSelectedItem();
        Task selectedTask3 = taskTable3.getSelectionModel().getSelectedItem();

        if (selectedTask3 != null) {
            mainApp.getTaskData2().add(selectedTask3);
            mainApp.getTaskData3().remove(selectedTask3);
            taskTable3.getSelectionModel().clearSelection();
        }
        else if(selectedTask2 != null){
            mainApp.getTaskData().add(selectedTask2);
            mainApp.getTaskData2().remove(selectedTask2);
            taskTable2.getSelectionModel().clearSelection();
        }
        else if(selectedTask != null){
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Task Can't Go Further Backwards");
            alert.setHeaderText("No More Progress Tables");
            alert.setContentText("Please select another task in the tables or move this task further");

            alert.showAndWait();
            taskTable.getSelectionModel().clearSelection();
        }
        else{
            noSelectionAlert();
        }
    }

    @FXML
    public void clearOtherSelected() {
        taskTable3.getSelectionModel().clearSelection();
        taskTable2.getSelectionModel().clearSelection();
    }

    @FXML
    public void clearOtherSelected2() {
        taskTable3.getSelectionModel().clearSelection();
        taskTable.getSelectionModel().clearSelection();
    }

    @FXML
    public void clearOtherSelected3() {
        taskTable.getSelectionModel().clearSelection();
        taskTable2.getSelectionModel().clearSelection();
    }

    private void noSelectionAlert(){
        // Nothing selected.
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.initOwner(mainApp.getPrimaryStage());
        alert.setTitle("No Selection");
        alert.setHeaderText("No Task Selected");
        alert.setContentText("Please select a task in the table");

        alert.showAndWait();
    }

    private void setNewTooltip(TableView<Task> table, TableColumn<Task, String> taskColumn){
        taskColumn.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Task, String> call(TableColumn<Task, String> p) {
                return new TableCell<>() {
                    @Override
                    public void updateItem(String t, boolean empty) {
                        super.updateItem(t, empty);
                        if (t == null) {
                            setTooltip(null);
                            setText(null);
                            setStyle(null);
                        } else {
                            Tooltip tooltip = new Tooltip();
                            Task task = table.getItems().get(getTableRow().getIndex());
                            if (task.getPriority().equals("HIGH")) {
                                setStyle("-fx-background-color: red");
                            }
                            else if(task.getPriority().equals("MEDIUM")){
                                setStyle("-fx-background-color: orange");
                            }
                            else setStyle("-fx-background-color: palegreen");
                            tooltip.setText(task.getTaskDescription());
                            tooltip.setMaxWidth(300);
                            tooltip.setWrapText(true);
                            setTooltip(tooltip);
                            setText(t);
                        }
                    }
                };
            }
        });
    }

    void setMainApp(Main mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        taskTable.setItems(mainApp.getTaskData());
        taskTable2.setItems(mainApp.getTaskData2());
        taskTable3.setItems(mainApp.getTaskData3());
    }


}
