package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.prefs.Preferences;

public class Main extends Application {

    private Stage primaryStage;
    private GridPane rootLayout;

    private ObservableList<Task> taskData = FXCollections.observableArrayList();
    private ObservableList<Task> taskData2 = FXCollections.observableArrayList();
    private ObservableList<Task> taskData3 = FXCollections.observableArrayList();

    public Main(){

        taskData.add(new Task("test1", "description1", "LOW", LocalDate.of(2019,4,9)));
        taskData2.add(new Task("test2", "description2", "MEDIUM", LocalDate.of(2019,5,10)));
        taskData3.add(new Task("test3", "description3", "HIGH", LocalDate.of(2019,6,11)));

    }

    ObservableList<Task> getTaskData() {
        return taskData;
    }

    ObservableList<Task> getTaskData2() {
        return taskData2;
    }

    ObservableList<Task> getTaskData3() {
        return taskData3;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Kanban");
        //this.primaryStage.setResizable(false);

        initRootLayout();

        showTaskOverview();
    }

    private void initRootLayout() throws Exception{
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("RootLayout.fxml"));
            rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            // Give the controller access to the main app.
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showTaskOverview() throws Exception{
        // Load person overview.
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("TaskOverview.fxml"));
        GridPane taskOverview = loader.load();

        // Set person overview into the center of root layout.
        rootLayout.addRow(1, taskOverview);

        // Give the controller access to the main app.
        TaskOverviewController controller = loader.getController();
        controller.setMainApp(this);
    }

    boolean showTaskEditDialog(Task task)throws Exception {
        // Load the fxml file and create a new stage for the popup dialog.
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("TaskEditDialog.fxml"));
        AnchorPane page = loader.load();

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Edit Task");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(page);
        dialogStage.setScene(scene);

        // Set the task into the controller.
        TaskEditDialogController controller = loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTask(task);

        // Show the dialog and wait until the user closes it
        dialogStage.setResizable(false);
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    Stage getPrimaryStage() {
        return primaryStage;
    }

    public void loadTaskDataFromFile(File file) {
        try {
            ArrayList<Task> ToDoArrayList;
            ArrayList<Task> inProgressArrayList;
            ArrayList<Task> DoneArrayList;

            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));
            ToDoArrayList = (ArrayList<Task>) inputStream.readObject();
            inProgressArrayList = (ArrayList<Task>) inputStream.readObject();
            DoneArrayList = (ArrayList<Task>) inputStream.readObject();

            taskData.clear();
            taskData.addAll(ToDoArrayList);

            taskData2.clear();
            taskData2.addAll(inProgressArrayList);

            taskData3.clear();
            taskData3.addAll(DoneArrayList);

            inputStream.close();
        } catch (ClassNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(getPrimaryStage());
            alert.setTitle("Loading error");
            alert.setHeaderText("Error occured during loading.");
            alert.setContentText("Error message: " + e.getMessage());

            alert.showAndWait();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(getPrimaryStage());
            alert.setTitle("Loading error");
            alert.setHeaderText("Error occured during loading!");
            alert.setContentText("Error message: " + e.getMessage());

            alert.showAndWait();
        }
    }

    public void saveTaskDataToFile(File file) {
        try {
            ArrayList<Task> ToDoArrayList;
            ArrayList<Task> inProgressArrayList;
            ArrayList<Task> DoneArrayList;

            if (taskData instanceof ArrayList<?>) {
                ToDoArrayList = (ArrayList<Task>) taskData;
            } else {
                ToDoArrayList = new ArrayList<>(taskData);
            }

            if (taskData2 instanceof ArrayList<?>) {
                inProgressArrayList = (ArrayList<Task>) taskData2;
            } else {
                inProgressArrayList = new ArrayList<>(taskData2);
            }

            if (taskData3 instanceof ArrayList<?>) {
                DoneArrayList = (ArrayList<Task>) taskData3;
            } else {
                DoneArrayList = new ArrayList<>(taskData3);
            }

            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
            outputStream.writeObject(ToDoArrayList);
            outputStream.writeObject(inProgressArrayList);
            outputStream.writeObject(DoneArrayList);
            outputStream.close();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(getPrimaryStage());
            alert.setTitle("Saving error");
            alert.setHeaderText("Error occured during saving.");
            alert.setContentText("Error message: " + e.getMessage());

            alert.showAndWait();
        }
    }

    public void importTaskDataFromFile(File file){
        try {
            Scanner scanner=new Scanner(file);
            String ln="";

            ObservableList<Task> ListToDo= FXCollections.observableArrayList();
            ObservableList<Task> ListInProcess=FXCollections.observableArrayList();
            ObservableList<Task> ListDone=FXCollections.observableArrayList();

            while(scanner.hasNextLine()) {

                ln=scanner.nextLine();

                if (!ln.equals("")) {
                    String[] tmpList = ln.split(";");

                    String listName = tmpList[0];
                    String taskName = tmpList[1];
                    String taskPriority = tmpList[2];
                    LocalDate expDate = LocalDate.parse(tmpList[3]);
                    String taskDescription = tmpList[4];

                    if((listName.equals("TO DO")||listName.equals("IN PROCESS")||listName.equals("DONE"))) {
                        Task tmp = new Task(taskName, taskDescription, taskPriority, expDate);

                        if (listName.equals("TO DO")) ListToDo.add(tmp);
                        else if (listName.equals("IN PROCESS")) ListInProcess.add(tmp);
                        else if(listName.equals("DONE")) ListDone.add(tmp);
                    }
                }
            }
            scanner.close();

            taskData.clear();
            taskData.addAll(ListToDo);

            taskData2.clear();
            taskData2.addAll(ListInProcess);

            taskData3.clear();
            taskData3.addAll(ListDone);

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(getPrimaryStage());
            alert.setTitle("Importing error");
            alert.setHeaderText("Error occured during importing.");
            alert.setContentText("Error message: " + e.getMessage());

            alert.showAndWait();
        }
    }

    public void exportTaskDataToFile(File file) {
        try {
            PrintWriter printWriter = new PrintWriter(file);

            if (taskData.size() != 0) {

                //printWriter.println("TO DO:");

                for(int i=1; i<=taskData.size(); i++){
                    Task tmp = taskData.get(i - 1);
                    printWriter.println("TO DO;" + tmp.getTaskName() + ";" + tmp.getPriority() + ";" + tmp.getExpDate().toString() + ";" + tmp.getTaskDescription());
                }

            }

            if (taskData2.size() != 0) {

                //printWriter.println("IN PROCESS:");

                for(int i=1; i<=taskData2.size(); i++){
                    Task tmp = taskData2.get(i - 1);
                    printWriter.println("IN PROCESS;"+tmp.getTaskName() + ";" + tmp.getPriority() + ";" + tmp.getExpDate().toString() + ";" + tmp.getTaskDescription());
                }

            }

            if (taskData3.size() != 0) {

                //printWriter.println("DONE:");

                for(int i=1; i<=taskData3.size(); i++){
                    Task tmp = taskData3.get(i - 1);
                    printWriter.println("DONE;" + tmp.getTaskName() + ";" + tmp.getPriority() + ";" + tmp.getExpDate().toString() + ";" + tmp.getTaskDescription());
                }

            }

            printWriter.close();

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(getPrimaryStage());
            alert.setTitle("Exporting error");
            alert.setHeaderText("Error occured during exporting.");
            alert.setContentText("Error message: " + e.getMessage());

            alert.showAndWait();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
